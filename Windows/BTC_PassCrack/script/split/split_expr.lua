local split={}

function split.split(line, add)
	--if true then return end
	
	add(line)
	
	local mats = {
		'([a-z]+)',
		'([A-Z]+)',
		'([0-9]+)',
		'([0-9]+)',
		'([^a-zA-Z0-9%s])',
		
		'[12][90]([0-9][0-9][01][0-9][0-3][0-9])',		-- 生日 -> 2位年+月日  
		'([12][90][0-9][0-9])[01][0-9][0-3][0-9]',		-- 生日 -> 年  
		'[12][90][0-9][0-9]([01][0-9][0-3][0-9])',		-- 生日 -> 月日  

		'[0-9][0-9]([01][0-9][0-3][0-9])[^0-9].',		-- 不完整生日 -> 月日  
		'[0-9][0-9]([01][0-9][0-3][0-9])$',				-- 不完整生日 -> 月日  

	};
	for _,mat in pairs(mats)do
		for w in line:gmatch(mat) do
			add(w)
		end
	end

end

return split