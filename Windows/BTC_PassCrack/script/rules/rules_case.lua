local rules={}

table.insert(rules, {
	rk = 'u',
	desc = '全部大写',
	name = 'case_upper_all',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		return src:upper()
	end
});
table.insert(rules, {
	rk = 'l',
	desc = '全部小写',
	name = 'case_lower_all',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		return src:lower()
	end
});

table.insert(rules, {
	rk = 't',
	desc = '反转大小写',
	name = 'case_toggle',
	next = function (src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ret = ''
		local add_upper
		for i=1, src:len() do
			local ch = src:sub(i,i)
			if ch>='a' and ch<='z' then
				ret = ret .. ch:upper()
			elseif ch>='A' and ch<='Z' then
				ret = ret .. ch:lower()
			else
				ret = ret .. ch
			end
		end
		return ret
	end,
});

table.insert(rules, {
	rk='c',
	desc = '首字大写其他小写',
	name = 'case_upper_first_lower_other',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(1,1)
		return ch:upper() .. src:sub(2):lower()
	end
});
table.insert(rules, {
	rk='C',
	desc = '首字小写其他大写',
	name = 'case_lower_first_upper_other',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(1,1)
		return ch:lower() .. src:sub(2):upper()
	end
});

table.insert(rules, {
	rk = '',
	desc = '首字大写',
	name = 'case_upper_first',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(1,1)
		if ch>='a' and ch<='z' then
			return ch:upper() .. src:sub(2)
		end
	end
});

table.insert(rules, {
	desc = '首字小写',
	name = 'case_lower_first',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(1,1)
		if ch>='a' and ch<='z' then
			return ch:lower() .. src:sub(2)
		end
	end
});

table.insert(rules, {
	desc = '尾字大写',
	name = 'case_upper_last',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(-1, -1)
		if ch>='a' and ch<='z' then
			return src:sub(1, -2) .. ch:upper()
		end
	end
});

table.insert(rules, {
	desc = '尾字小写',
	name = 'case_lower_last',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		ctx.eof = true

		local ch = src:sub(-1, -1)
		if ch>='a' and ch<='z' then
			return src:sub(1, -2) .. ch:lower()
		end
	end
});

local _rules = table.clone(rules)
table.insert(rules, {
	desc = '全部大小写变换',
	name = 'case_all',
	next = function(src, ctx)
		if ctx.eof then
			return
		end
		if( not ctx.i ) then
			ctx.i = 1
		end
		if not ctx.sub then
			ctx.sub={}
		end
		while ctx.i <= #_rules do
			local str = _rules[ctx.i].next(src, ctx.sub)
			if str and str ~= src then
				return str
			end
			ctx.i = ctx.i + 1
			ctx.sub={}
		end
		ctx.sub = nil
		ctx.eof = true
	end
});


return {
	desc = '大小写变换',
	rules = rules,
}