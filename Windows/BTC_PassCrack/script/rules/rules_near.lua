
local TMPL = {
	['1']={-- near en key top
		'`','2','q','w','Q','W', 
		-- near right num key
		'4','5','2','0',},
	['2']={-- near en key top
		'1','3','q','w','e','Q','W','E',
		-- near right num key
		'4','5','6','1','3','0','.',},
	['3']={-- near en key top
		'2','4','w','e','r','W','E','R',
		-- near right num key
		'5','6','+','2','0','.',},
	['4']={-- near en key top
		'3','5','e','r','t','E','R','T',
		-- near right num key
		'7','8','5','1','2',},
	['5']={-- near en key top
		'4','6','r','t','y','R','T','Y',
		-- near right num key
		'7','8','9','4','6','1','2','3',},
	['6']={-- near en key top
		'5','7','t','y','u','T','Y','U',
		-- near right num key
		'8','9','5','+','2','3',},
	['7']={-- near en key top
		'6','8','y','u','i','Y','U','I',
		-- near right num key
		'/','8','4','5',},
	['8']={-- near en key top
		'7','9','u','i','o','U','I','O',
		-- near right num key
		'/','*','7','9','4','5','6',},
	['9']={-- near en key top
		'8','0','i','o','p','I','O','P',
		-- near right num key
		'/','*','-','9','8','+','5','6',},
	['0']={-- near en key top
		'9','-','o','p','[','O','P',},
	
	['q']={'`','1','2','w','a','s',},
	['w']={'1','2','3','q','e','a','s','d',},
	['e']={'2','3','4','w','r','s','d','f',},
	['r']={'3','4','5','e','t','d','f','g',},
	['t']={'4','5','6','r','y','f','g','h',},
	['y']={'5','6','7','t','u','g','h','j',},
	['u']={'6','7','8','y','i','h','j','k',},
	['i']={'7','8','9','u','o','j','k','l',},
	['o']={'8','9','0','i','p','k','l',';',},
	['p']={'9','0','-','o','[','l',';','\'',},
	['a']={'q','w','s','z','x',},
	['s']={'q','w','e','a','d','z','x','c',},
	['d']={'w','e','r','s','f','x','c','v',},
	['f']={'e','r','t','d','g','c','v','b',},
	['g']={'r','t','y','f','h','v','b','n',},
	['h']={'t','y','u','g','j','b','n','m',},
	['j']={'y','u','i','h','k','n','m',',',},
	['k']={'u','i','o','j','l','m',',','.',},
	['l']={'i','o','p','k',';',',','.','/',},
	['z']={'a','s','x',},
	['x']={'a','s','d','z','c',},
	['c']={'s','d','f','x','v',},
	['v']={'d','f','g','c','b',},
	['b']={'f','g','h','v','n',},
	['n']={'g','h','j','b','m',},
	['m']={'h','j','k','n',',',},
	['Q']={'`','1','2','W','A','S',},
	['W']={'1','2','3','Q','E','A','S','D',},
	['E']={'2','3','4','W','R','S','D','F',},
	['R']={'3','4','5','E','T','D','F','G',},
	['T']={'4','5','6','R','Y','F','G','H',},
	['Y']={'5','6','7','T','U','G','J','K',},
	['U']={'6','7','8','Y','I','H','J','K',},
	['I']={'7','8','9','U','O','J','K','L',},
	['O']={'8','9','0','I','P','K','L',';',},
	['P']={'9','0','-','O','[','L',';','\'',},
	['A']={'Q','W','S','Z','X',},
	['S']={'Q','W','E','A','D','Z','X','C',},
	['D']={'W','E','R','S','F','X','C','V',},
	['F']={'E','R','T','D','G','C','V','B',},
	['G']={'R','T','Y','F','H','V','B','N',},
	['H']={'T','Y','U','G','J','B','N','M',},
	['J']={'Y','U','I','H','K','N','M',',',},
	['K']={'U','I','O','J','L','M',',','.',},
	['L']={'I','O','P','K',';',',','.','/',},
	['Z']={'A','S','X',},
	['X']={'A','S','D','Z','C',},
	['C']={'S','D','F','X','V',},
	['V']={'D','F','G','C','B',},
	['B']={'F','G','H','V','N',},
	['N']={'G','H','J','B','M',},
	['M']={'H','J','K','N',',',},
};

local function transform(src, ctx)
	if not ctx.x then
		ctx.x = 1
		ctx.y = 1
	end
	
	local srclen = src:len()
	while ctx.x <= srclen do
		repeat
			local ch = src:sub(ctx.x, ctx.x)
			local map = TMPL[ch]
			local count = 0
			if map then
				count = #map;
			else
				count = 0;
--				print('invalid char', ch)
			end
			if ctx.y <= count then
				local newkey = src:sub(1, ctx.x-1) .. map[ctx.y] .. src:sub(ctx.x+1)
				ctx.y = ctx.y + 1
				assert(newkey)
				return newkey 
			else
				local z = ctx.y - count
				ctx.y = ctx.y + 1
				if z==1 then
					local newkey = src:sub(1, ctx.x) .. ch .. src:sub(ctx.x+1)
					return newkey 
				elseif z==2 then
					local newkey = src:sub(1, ctx.x) .. ch .. ch .. src:sub(ctx.x+1)
					return newkey 
				elseif z==3 then
					local newkey = src:sub(1, ctx.x-1) .. src:sub(ctx.x+1)
					return newkey 
				elseif z==4 and ctx.x+1<=srclen then
					local newkey = src:sub(1, ctx.x-1) .. src:sub(ctx.x+2)
					return newkey 
				end
				
				break; --> continue.next
			end
		until(false);
		ctx.x = ctx.x + 1
		ctx.y = 1
	end
	
	ctx.y = nil -- free
	--ctx.x = nil -- reset next state
end

local function calc_count(src)
	local total = 0
	for i=1, src:len() do
		local ch = key:sub(i, i)
		local map = TMPL[ch]
		if map then
			total = total + #map
		end
		total = total + 4 -- count(z)
	end
	return total
end

local rules = {}
table.insert(rules, {
	desc = '手误',
	name = 'input_error',
	--max_count = max_count,
	next = transform	
});

return {
	desc = '手误变换',
	rules = rules,
}