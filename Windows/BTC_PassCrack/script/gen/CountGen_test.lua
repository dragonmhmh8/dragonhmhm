local CountGen = require('CountGen')

------------------------------------------------------------------------------
local function test_CountGenEx(info)	
	local ctx={}
	local obj={}
	CountGen.init(obj, info, ctx)
	local runcount = 0
	local exist = {}
	while true do
		local ret = CountGen.next(obj, ctx)
		if not ret then
			break
		end
		runcount = runcount + 1
		local str = table.dump(ret)
		if exist[str] then
			var_dump(info)
			assert(false, "exist ret="..str)
		end
		exist[str] = true
		print('id', str)
	end
	
	local sumMul = 1
	for k,v in pairs(info) do
		sumMul = sumMul * (v.max-v.min+1)
	end
	if runcount ~= sumMul then
		var_dump(info)
		assert(false, 'run='..runcount..'/sumMul='..sumMul)
	end
	return sumMul == runcount
end

------------------------------------------------------------------------------
local function test_CountGenRand()
	math.randomseed(os.time()+math.random())
	local info = {}-- { {min=0, max=1},{min=0, max=3},{min=0, max=1} }
	for _=1, 10000 do
		print('==================')
		info={}
		for i=1, math.random(2,5) do
			info[i] = { min=math.random(0,5) }
			info[i].max = math.random(info[i].min, 8)
		end
		if not test_CountGenEx(info) then
			break
		end
	end
end

if TEST_MODE or TEST_TARGET=='count' then


	test_CountGenEx({{min=1,max=3},{min=0,max=3}})
	print('===========')
	test_CountGenEx({{min=0,max=5},{min=4,max=4}})
	print('===========')
	test_CountGenEx({{min=5,max=7},{min=4,max=8},{min=1,max=2}})
	print('===========')
	test_CountGenEx({{min=0,max=1},{min=0,max=5},{min=2,max=6}})
	print('===========')
	test_CountGenEx({{min=4,max=4},{min=3,max=3}})
	print('===========')
	test_CountGenEx({{min=3,max=4},{min=0,max=1},{min=0,max=3}})
	print('===========')
	test_CountGenEx({{min=0,max=5},{min=0,max=3}})
	print('===========')

	test_CountGenRand()

	print('test finish')

end


------------------------------------------------------------------------------
return CountGen
