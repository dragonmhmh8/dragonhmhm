require('base')

assert(SCRIPT_ROOT);
package.path=SCRIPT_ROOT..'words/?.lua;'..package.path;

local CountGen=require('CountGen')
local expr_to_holder=require('expr2hold').expr_to_holder
local MAX_COUNT = 88
------------------------------------------------------------------------------

local gen = {}
function gen:init(opt, ctx)
	assert(opt.expr)
	self.maxlen = opt.maxlen or MAX_COUNT
	assert(self.maxlen>=1)
	self.opt = opt
	local holder = expr_to_holder(opt.expr)
	self.holder = holder
	if not self.rslist then
		self.rslist = {}
		for h,hold in pairs(holder) do
			self.rslist[h] = {}
			local maxmaxcount = nil
			local minminlen = nil
			for si, sitem in pairs(hold.select) do
				local rs = {}
				if not sitem.from and sitem.keys and #sitem.keys then
					sitem.from = 'raw'
					sitem.param = sitem.keys
				end
				--var_dump(sitem)
				assert(sitem.from)
				local m = require('words_'..sitem.from)
				rs.module = m
				local tmpobj={}
				m.init(tmpobj, sitem.param)
				rs.total, rs.minlen = m.total(tmpobj)
				if not minminlen or rs.minlen < minminlen then
					minminlen = rs.minlen
				end
				local maxcount = self.maxlen / math.max(rs.minlen or 1, 1)
				maxcount = math.floor(maxcount)
				--maxcount = math.ceil( maxcount )
				if not maxmaxcount or maxcount > maxmaxcount then
					maxmaxcount = maxcount
				end
				rs._cache = { count = hold.count, param=sitem.param, maxsi=#hold.select }
				if not rs.total then
					assert(false, "cannot calc words count " .. tostring(json_encode(sitem)) )
				end
				self.rslist[h][si] = rs
			end
			if maxmaxcount then
				hold.count.max = math.min(maxmaxcount, hold.count.max)
			end
			hold.count.minminlen = minminlen
		end
		--[[
		rslist={
			[hold.i]={
				[select.item]={
					module=,
					total=xxx,
					_cache={count=hold.select.count, param=hold.select.module.param, maxsi=#hold.select}
				}
			},{...}
		}
		]]
	end
	self._maxcount = 1
	local allzero = true
	for _, h in pairs(holder) do
		if h.count.min > 0 then
			allzero = false
			break
		end
	end
	for h,itemlist in pairs(self.rslist) do
		local elementCount = 0
		for _, item in pairs(itemlist) do
			elementCount = elementCount + item.total
		end
		local hold = holder[h]
		local elementSize = 0
		for i=hold.count.min, hold.count.max do
			elementSize = elementSize + math.pow(elementCount, i)
		end
		self._maxcount = self._maxcount * elementSize
	end
	if allzero then
		self._maxcount = self._maxcount - 1
	end


	if not self.mobjs then
		self.mobjs = {}
		for h=1, #holder do
			self.mobjs[h] = {}
		end
	end
	
	
	local ciinfo = {}
	for _1, h in pairs(holder) do
		table.insert(ciinfo, h.count)
	end
	if not ctx.ctxCount then
		ctx.ctxCount = {}
	end
	self.countMgr = {}
	CountGen.init(self.countMgr, ciinfo, ctx.ctxCount)

	if VERBOSE then
		var_dump(self)
	end
		
	--[[
		idx={
			[hold.i]={
				[nowcount.0]={
					si = number -> hold.select[si] -> for(si,item in hold.select),
					id = id range keys.total
				},[nowcount.n]={...}
			},{...}
		},
		nowcount={
			[hold.i]=nowcount
		},
	]]

	if VERBOSE then
		var_dump(ctx)
	end

	if not ctx.idx then
		ctx.idx = {}
	end
end

------------------------------------------------------------------------------
function gen:next(ctx)
	while true do
		local a,b,c,d = gen.next2(self, ctx)
		if not a then
			break
		end
		if a:len() <= self.maxlen then
			return a,b,c,d
		end
	end
end
function gen:next2(ctx)
	if ctx.eof then
		return
	end

	local countAdjusted = false
	if not ctx.nowcount then
		countAdjusted = true
	end
	local h = 1		-- .hold
	local ci = 1	-- count.expand[ci]
	if ctx.nowcount then
		while ctx.nowcount[h]==0 and h<=#ctx.nowcount do
			h = h + 1
		end
	end
	while true do
		if countAdjusted then
			countAdjusted = false
			repeat
				ctx.nowcount = CountGen.next(self.countMgr, ctx.ctxCount)
				if not ctx.nowcount then
					break
				end
				local minlen = 0
				for i=1, #ctx.nowcount do
					minlen = minlen + self.holder[i].count.minminlen * ctx.nowcount[i]
				end
			until(not (minlen==0 or minlen>self.maxlen) )
			if not ctx.nowcount then
				break
			end
			for _h=1, #self.holder do
				if not ctx.idx[_h] then
					ctx.idx[_h] = {}
				end
				for _ci=1, ctx.nowcount[_h] do
					if not ctx.idx[_h][_ci] then
						ctx.idx[_h][_ci] = { si=1,id=1 }
					end
				end
			end
			h = 1
			while ctx.nowcount[h]==0 and h<=#ctx.nowcount do
				h = h + 1
			end
			ci = 1
			for i=1, #ctx.idx do
				if #ctx.idx[i] > 0 and ctx.nowcount[i]~=0 then
					ctx.idx[i][1].id = 0;
					h = i
					break
				end
			end
		end -- end countAdjusted 

		local data = ctx.idx[h][ci]
		if not data then
			data = { si=1, id=1-1 }
			ctx.idx[h][ci] = data
		end
		-- {si=?,id=?}
		local item = self.rslist[h][data.si]
		data.id = data.id + 1
		if data.id <= item.total then
			break
		end
		data.id = 0
		data.si = data.si + 1
		if data.si > #self.rslist[h] then
			data.id = 1
			data.si = 1
			ci = ci + 1
			if ci > ctx.nowcount[h] then
				ci = 1
				repeat
					h = h + 1
				until not (h <= #self.rslist and ctx.nowcount[h]==0);
				if h > #self.rslist then
					countAdjusted = true
				end
			end
		end
	end
	if not ctx.nowcount then
		ctx.eof = true
		return false
	end
	
	local ret = ''
	local all_keys = {}
	local hold2itemidx = {}
	for h=1, #self.holder do
		local itemidx = {}
		for ci=1, ctx.nowcount[h] do
			local v = ctx.idx[h][ci]
			local item = self.rslist[h][v.si]
			
			local obj1 = self.mobjs[h][ci]
			if not obj1 then
				obj1 = {}
				self.mobjs[h][ci] = obj1
			end
			local obj2 = obj1[v.si]
			if not obj2 then
				obj2 = {}
				item.module.init(obj2, item._cache.param)
				obj1[v.si] = obj2
			end
			assert(v.id >= 1 and v.id <= item.total)
			local k = item.module.at(obj2, v.id) or ''
			if k:len() > 0 then
				ret = ret..k
				table.insert(all_keys, k)
				table.insert(itemidx, #all_keys)
			end
		end
		hold2itemidx[h] = itemidx
	end
	return ret, all_keys, hold2itemidx
end

function gen:hash()
	local hash = ''
	for _, hold in pairs(self.rslist) do
		for _, item in pairs(hold) do
			local obj = {}
			item.module.init(obj, item._cache.param)
			local hashval = item.module.hash(obj)
			hash = hash .. hashval .. '|'
		end
	end
	hash = md5(hash)
    local data = {
        ver = 20180520161800,
        expr = self.opt.expr,
		maxlen = self.opt.maxlen,
		sub = hash,
    }
    return md5(json_encode(data))
end
------------------------------------------------------------------------------


return gen