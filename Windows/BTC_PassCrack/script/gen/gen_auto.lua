require('base')

package.path=SCRIPT_ROOT..'rules/?.lua;'..package.path;

local MAX_COUNT = 88

assert(file_search)
assert(path_basename)
------------------------------------------------------------------------------
local LimitSet={}
function LimitSet.init(this, limit)
    this.vals={}
    this.limit = limit
    this.order = {}
end
function LimitSet.insert(this, val)
    if this.vals[val] then
        return
    end
    if #this.order>=this.limit then
        local k = this.order[1]
        table.remove(this.order, 1)
        this.vals[k] = nil
    end
    this.vals[val] = true
    table.insert(this.order, val)
end
function LimitSet.has(this, val)
    return this.vals[val] ~= nil
end
------------------------------------------------------------------------------
local gen={}

local function load_all_rules()
    local files = file_search(SCRIPT_ROOT..'rules')
    local name2rule = {}
    for _,path in pairs(files) do
        local basename = path_basename(path)
        assert(basename)
        local m = require(basename)
        if m and m.rules then
            for _, rule in pairs(m.rules) do
                if rule.name and rule.next then
                    name2rule[rule.name] = rule
                end
            end
        end
    end
    local sorted_ret = {}
    for k,v in pairs(name2rule) do
        if v.name ~= 'all' then
            table.insert(sorted_ret, v)
        end
    end
    table.sort(sorted_ret, function(a,b)
        return a.name < b.name
    end)

    name2rule['all'] = {
        name = 'all',
        next = function(src, ctx)
            if ctx.eof then
                return
            end
            if not ctx.i then
                ctx.i = 1
            end
            if not ctx.sub then
                ctx.sub={}
            end
            while ctx.i<=#sorted_ret do
                local rule = sorted_ret[ctx.i]
                if not rule then
                    ctx.eof = true
                    return
                end
                local ret = rule.next(src, ctx.sub)
                if ret and ret~=src then
                    return ret
                end
                ctx.sub = {}
                ctx.i = ctx.i + 1
            end
            ctx.eof = true
            ctx.sub = nil
        end
    }
    return name2rule
end

local ruleTarget = {
    hold = 1,
    element = 2,
    all_hold = 3,       -- 表达式组合结果[xx]{m,n}
    all_element = 4,    -- 表达式组合元素[xx]{1}
    result_string = 5,
}
local function filter_rule(opt, name2rule)
    local ret = {}
    if opt.rule_expr then
        local char2rule = {}
        for name,rule in pairs(name2rule) do
            if rule.rk and rule.rk:len()>0 then
                char2rule[rule.rk] = rule
            end
        end
        if type(opt.rule_expr)=='string' then
            opt.rule_expr = {opt.rule_expr}
        end
        for _, expr in pairs(opt.rule_expr) do
            local cmds = {}
            for i=1,#expr do
                local ch = opt.rule_expr:sub(i,i)
                local rule = char2rule[ch]
                if rule then
                    table.insert(cmds, {rule=rule, target=-1})
                else
                    assert(false, 'invalid rule char ' .. ch)
                end
            end
            if #cmds > 0 then
                table.insert(ret, cmds)
            end
        end
    end

    if opt.rule_register then
        for name, vlist in pairs(opt.rule_register) do
            assert(not name2rule[name])
            if type(vlist)=='string' then
                vlist = string.split(vlist, '+')
            end
            local _rules = {}
            for _,v in pairs(vlist) do
                local rule = name2rule[v]
                if not rule and v~='all' then
                    _rules = {}
                    break
                end
                table.insert(_rules, rule)
            end
            name2rule[name] = {
                desc='合并'..name,
                name=name,
                next=function(src, ctx)
                    if ctx.eof then
                        return
                    end
                    if( not ctx.i ) then
                        ctx.i = 1
                    end
                    if not ctx.sub then
                        ctx.sub={}
                    end
                    while ctx.i <= #_rules do
                        local str = _rules[ctx.i].next(src, ctx.sub)
                        if str and str ~= src then
                            return str
                        end
                        ctx.i = ctx.i + 1
                        ctx.sub={}
                    end
                    ctx.sub = nil
                    ctx.eof = true
                end,
            }
        end
    end

    if opt.rule_cmd_list then
        if type(opt.rule_cmd_list)=='string' then
            opt.rule_cmd_list = string.split(opt.rule_cmd_list, '+')
        end
		assert(type(opt.rule_cmd_list)=='table')
		for k,subary in pairs(opt.rule_cmd_list) do
			if type(subary)=='string' then
				opt.rule_cmd_list[k] = string.split(subary, '+')
			end
		end
        for _, cmd_names in pairs(opt.rule_cmd_list) do
            local cmds = {}
            for _, name in pairs(cmd_names) do
                local rule = name2rule[name]
                local target = 0
                local targetType = ruleTarget.all_element
                if not rule then
                    -- xxx(123)
                    local tmpName, tmpTarget = name:gmatch([[([^%(]+)%(([^%)]+)%)]])()
                    if tmpName and tmpName:len()>0 then
                        name = tmpName
                    end
                    if tmpTarget and tmpTarget:len()>0 then
                        if tmpTarget:sub(1,1)=='#' then
                            -- xxx(#123) -> expr.element
                            target = tonumber(tmpTarget:sub(2))
                            targetType = ruleTarget.element
                        else
                            -- xxx(123) -> expr(N)
                            target = tonumber(tmpTarget)
                            targetType = ruleTarget.hold
                        end
                    end
                    rule = name2rule[name]
                end
                assert(rule, 'invalid rule name ' .. name)
                assert(rule.next)
                table.insert(cmds, {rule=rule, target=target or 0, targetType=targetType })
            end
            if #cmds > 0 then
                table.insert(ret, cmds)
            end
        end
    end
    return ret
end

function gen:init(opt, ctx)
    self.opt = opt
    self.maxlen = opt.maxlen or MAX_COUNT
    assert(self.maxlen>=1)
    
    if opt.expr then
        self.implClass = require('gen_expr')
    elseif opt.module then
        self.implClass = require('gen_single')
    else
        return error('invalid option')
    end

    self.rule_cmd_list = filter_rule(opt, load_all_rules())

    if not self.implObj then
        self.implObj = {}
    end
    if not ctx.implCtx then
        ctx.implCtx = {}
    end
	
    self.implClass.init(self.implObj, opt, ctx.implCtx)
    if not self.diffset then
        self.diffset = {}
        LimitSet.init(self.diffset, 300)    
    end
end
local ruleMgr={}
function ruleMgr:next(ctx, info)
    local now_all_keys = ctx.now_all_keys
    local valid_cmds = ctx.valid_cmds
    if not now_all_keys or not valid_cmds then
        valid_cmds = {}
        now_all_keys = table.clone(info.all_keys)
        ctx.now_all_keys = now_all_keys
        ctx.now_all_key_last = info.last_pass
        local elementList = {}
        local idx2count = {}
        local lastTargetType = nil
        for cmd_index,rule in pairs(info.cmdlist) do
            if rule.targetType==ruleTarget.all_hold or rule.targetType==ruleTarget.hold then
                assert(lastTargetType == nil or lastTargetType == ruleTarget.hold)
                lastTargetType = ruleTarget.hold
                assert(info.hold2itemidx)
                local new_keys = {}
                for h,itemidxList in pairs(info.hold2itemidx) do
                    local holdStr = ''
                    for _,idx in pairs(itemidxList) do
                        assert(idx>=1 and idx <= #now_all_keys)
                        holdStr = holdStr .. now_all_keys[idx]
                    end
                    table.insert(new_keys, holdStr)
                end
                now_all_keys = new_keys
                ctx.now_all_keys = now_all_keys
                ctx.now_all_key_last = info.last_pass
            elseif rule.targetType==ruleTarget.all_element or rule.targetType==ruleTarget.element then
                assert(lastTargetType == nil or lastTargetType == ruleTarget.element)
                lastTargetType = ruleTarget.element
            else
                assert(false, 'invalid rule target type')
            end
            
            if rule.targetType==ruleTarget.all_hold or rule.targetType == ruleTarget.all_element then
                for x=1, #now_all_keys do
                    table.insert(elementList, {element_index=x, cmd_index=cmd_index})
                end
            elseif rule.targetType==ruleTarget.hold or rule.targetType==ruleTarget.element then                
                local target = rule.target
                if target <= -1 then
                    target = #now_all_keys + target + 1
                end
                table.insert(elementList, {element_index=target, cmd_index=cmd_index})
            end
        end

        -- filter bad rule
        for _, v in pairs(elementList) do
            local target = v.element_index
            assert(target~=0)
            if now_all_keys[target] then
                table.insert(valid_cmds, v)
                idx2count[target] = (idx2count[target] or 0) + 1
                assert(idx2count[target] == 1, '暂时不支持叠加变换')
            end
        end
        ctx.valid_cmds = valid_cmds

    end
    
    if #valid_cmds <= 0 then
        return
    end
    local rule_idx = 1
    if not ctx.sublist then
        ctx.sublist={}
        --ctx.now_all_keys={}
        for i=1, #valid_cmds do
            ctx.sublist[i] = {}
        end
        rule_idx = 1
    else
        rule_idx = #valid_cmds
    end

    local last_pass = info.last_pass;

    local isreset={}
    local changed = false
    while rule_idx <= #valid_cmds do
        local rs = valid_cmds[rule_idx]
        local element_index = rs.element_index
        local cmd = info.cmdlist[rs.cmd_index]
        assert(cmd)

        local str = ''
        if(element_index == 0)then
            str = last_pass
        else
            str = now_all_keys[element_index]
        end
        assert(str)

        local newstr
        while true do
            if str:len() >= (info.rule_minlen or 2) then
                assert(rule_idx>=1 and rule_idx <= #ctx.sublist)
                newstr = cmd.rule.next(str, ctx.sublist[rule_idx])
            end
            if not newstr or (newstr ~= str and newstr~='') then
                break
            end
        end
        
        if newstr then
            changed = true

            if element_index ~= 0 then
                now_all_keys[element_index] = newstr
                local newstr2 = ''
                for k=1,#now_all_keys do
                    newstr2 = newstr2 .. now_all_keys[k]
                end
                last_pass = newstr2
            else
                last_pass = newstr
            end

        elseif not isreset[rule_idx] then
            --now_all_keys[element_index]
            if rule_idx >= 2 then
                ctx.sublist[rule_idx] = {}
                isreset[rule_idx] = true
                rule_idx = rule_idx - 2 
            else
                ctx.sublist = nil
                break
            end
        end
        rule_idx = rule_idx + 1
    end
    if changed then
        return last_pass
    end
end

function gen:next2(ctx)
    if ctx.eof then
        return
    end
    if ctx.all_keys then
        while ctx.cmdlist_idx <= #self.rule_cmd_list do
            local cmdlist = self.rule_cmd_list[ctx.cmdlist_idx]
            local info = {
                cmdlist=cmdlist, 
                last_pass=ctx.last_pass,
                all_keys=ctx.all_keys, 
                hold2itemidx=ctx.hold2itemidx,
                rule_minlen=self.opt.rule_minlen                
            }
            local ret
            while true do
                ret = ruleMgr.next({}, ctx.ruleCtx, info)
                if not ret then
                    break
                end
                if not LimitSet.has(self.diffset, ret) then
                    LimitSet.insert(self.diffset, ret)
                    return ret
                end
            end
            ctx.cmdlist_idx = ctx.cmdlist_idx + 1
            ctx.ruleCtx = {}
        end
        ctx.y = nil
        ctx.all_keys = nil
    end
    while true do
        local pass,all_keys,hold2itemidx,d = self.implClass.next(self.implObj, ctx.implCtx)
        if not pass then
            ctx.eof = true
            return
        end
        if not LimitSet.has(self.diffset, pass) then
            LimitSet.insert(self.diffset, pass)

            ctx.cmdlist_idx = 1
            ctx.ruleCtx = {}
        
            ctx.all_keys = all_keys
            ctx.last_pass = pass
            ctx.hold2itemidx = hold2itemidx
            return pass, all_keys, hold2itemidx, d
        end
        --print('impl same value ', pass)
    end
end
function gen:next(ctx)
	while true do
		local a,b,c,d = gen.next2(self, ctx)
		if not a then
			break
		end
		if a:len() <= self.maxlen then
			return a,b,c,d
		end
	end
end
function gen:hash()
    local data = {
        -- ver = 20180520161800,
        ver = 20190607002802,
        rule_expr = self.opt.rule_expr,
        rule_cmd_list = self.opt.rule_cmd_list,
        expr = self.opt.expr,
        module = self.opt.module,
        maxlen = self.opt.maxlen,
        rule_minlen = self.opt.rule_minlen,
        sub = self.implClass.hash(self.implObj),
        ver_rule = 201808062302,
    }
    return md5(json_encode(data))
end
------------------------------------------------------------------------------


return gen